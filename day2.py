from aocd import data

data = data.split('\n')

def day_2A(data):
    out = []
    for i in data:
        a , b = i.split(' ')
        out.append((a, b))

    game = {
        'rock': ['A', 'X'],
        'paper': ['B', 'Y'],
        'scissors': ['C', 'Z']
    }

    score = 0
    for i, j in out:
        if i in game['scissors'] and j in game['rock']:
            score += 7 
        elif i in game['rock'] and j in game['paper']:
            score += 8
        elif i in game['paper'] and j in game['scissors']:
            score += 9

        if i in game['paper'] and j in game['rock']:
            score += 1 
        elif i in game['scissors'] and j in game['paper']:
            score += 2
        elif i in game['rock'] and j in game['scissors']:
            score += 3

        if i in game['rock'] and j in game['rock']:
            score += 4 
        elif i in game['paper'] and j in game['paper']:
            score += 5
        elif i in game['scissors'] and j in game['scissors']:
            score += 6

    return score

print(day_2A(data))

def day_2B(data):
    out = []
    for i in data:
        a , b = i.split(' ')
        out.append((a, b))

    game = {
        'lose': ['X'],
        'draw': ['Y'],
        'win': ['Z'],
        'rock': ['A'],
        'paper': ['B'],
        'scissors': ['C']
    }

    score = 0
    for i, j in out:
        if i in game['rock'] and j in game['lose']:
            score += 3
        elif i in game['rock'] and j in game['draw']:
            score += 4
        elif i in game['rock'] and j in game['win']:
            score += 8

        if i in game['paper'] and j in game['lose']:
            score += 1
        elif i in game['paper'] and j in game['draw']:
            score += 5
        elif i in game['paper'] and j in game['win']:
            score += 9

        if i in game['scissors'] and j in game['lose']:
            score += 2
        elif i in game['scissors'] and j in game['draw']:
            score += 6
        elif i in game['scissors'] and j in game['win']:
            score += 7

    return score

print(day_2B(data))



# Rock = [A, X, 1]
# paper = [B, Y, 2]
# scissors = [C, Z, 3]

# A > C > B > A
# X > Z > Y > X

# win = 6
# draw = 3
# lose = 0


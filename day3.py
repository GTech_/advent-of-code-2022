from aocd import data
import string

data = data.split('\n')

# a-z = 1-26
# A_Z = 27-52
    # 1 common character each?
#save commons in list
#sum up the list

#create string of letters
letters = string.ascii_lowercase + string.ascii_uppercase
letter_values = {}
value = 1

#give values to letters
for c in letters:
    letter_values[c] = value
    value += 1

def day_3A(data, letter_values):
    common = []

    # iterate through list of rucksacks and split them in half
    for rucksack in data:
        half = len(rucksack) // 2
        a = rucksack[:half]
        b = rucksack[half:]
        common_letter = ''

    # compare the two rucksacks and look for common characters
        for c in a:
            if c in b and not common_letter:
                common_letter = c
                #add common letters to list
                common.append(letter_values[common_letter])

    #sum up the common values
    return sum(common) 


def day_3B(data, letter_values):
    count = 0
    res = []

    # repeat while count is less than length of data, increasing count by 3(groups of 3 elves)
    while count < len(data):
        value = get_value(data, count, letter_values)
        res.append(value)
        # print(res)
        count += 3

    #sum up the common values
    return sum(res)

def get_value(data, count, letter_values):
    for c in data[count]:
        if c in data[count+1] and c in data[count+2]:
            # print('count', count)
            return letter_values[c]




print(day_3A(data, letter_values))
print(day_3B(data, letter_values))

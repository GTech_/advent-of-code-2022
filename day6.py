from aocd import data



def day_6A(data):
    for i in range(len(data)-4):
        check = set()
        for j in data[i:i+4]:
            check.add(j)
        if len(check) == 4:
            return(i+4)


def day_6B(data):
    for i in range(len(data)-14):
        check = set()
        for j in data[i:i+14]:
            check.add(j)
        if len(check) == 14:
            return(i+14)


# print(day_6A(data))
print(day_6B(data))

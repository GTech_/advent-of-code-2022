from aocd import data

data = data.split('\n')

stacks = [
    ['N', 'S', 'D', 'C', 'V', 'Q', 'T'],
    ['M', 'F', 'V'],
    ['F', 'Q', 'W', 'D', 'P', 'N', 'H', 'M'],
    ['D', 'Q', 'R', 'T', 'F'],
    ['R', 'F', 'M', 'N', 'Q', 'H', 'V', 'B'],
    ['C', 'F', 'G', 'N', 'P', 'W', 'Q'],
    ['W', 'F', 'R', 'L', 'C', 'T'],
    ['T', 'Z', 'N', 'S'],
    ['M', 'S', 'D', 'J', 'R', 'Q', 'H', 'N'] 
]
 

moves = []
for elem in data[10:]:
    elem = elem.split(' ')
    move= []
    move.append(elem[1])
    move.append(elem[3])
    move.append(elem[5])
    moves.append(move)


# def day_5A(stacks, moves):
#     for move in moves:
#         i = 0
#         while i < int(move[0]):
#             current = stacks[int(move[1])-1].pop()
#             stacks[int(move[2])-1].append(current)
#             i += 1

#     res = ''

#     for stack in stacks:
#         current = stack.pop()
#         res += current

#     return res

def day_5B(stacks, moves):
    # moves elements around in the stacks
    for move in moves:
        num_moving = int(move[0])
        while num_moving > 0:
            print('before', stacks)
            current = stacks[int(move[1])-1].pop(-num_moving)
            stacks[int(move[2])-1].append(current)
            num_moving -= 1
            print('after', stacks)

    res = ''

    # retrieves each top level element of the stacks
    for stack in stacks:
        current = stack.pop()
        res += current

    return res

# print(day_5A(stacks, moves))    
print(day_5B(stacks, moves))    
        
from aocd import data


data = data.split('\n')
res = []

#create a list of tuples
for c in data:
    a, b = c.split(',')
    res.append((a, b))
# print(res)
# grab first and second number in the tuple and create a range for each

def day_4A(res):
    count = 0

    for c in res:
        # separate
        a, b = c
        #create 2 ranges
        range_1 = []
        range_2 = []
        #grab first and second number
        num_1 = ''
        num_2 = ''
        num_3 = ''
        num_4 = ''
        count_1 = False
        count_2 = False

        for i in a:
            if i == '-':
                count_1 = True
            elif i.isdigit() and not count_1:
                num_1 += i
            elif i.isdigit() and count_1:
                num_2 += i
        num_1 = int(num_1)
        num_2 = int(num_2)
        for i in range(num_1, num_2+1):
            range_1.append(i)

        for i in b:
            if i == '-':
                count_2 = True
            elif i.isdigit() and not count_2:
                num_3 += i
            elif i.isdigit() and count_2:
                num_4 += i
        num_3 = int(num_3)
        num_4 = int(num_4)
        for i in range(num_3, num_4+1):
            range_2.append(i)

        # check if first range in second range or vice versa
        if all(c in range_1 for c in range_2) or all(c in range_2 for c in range_1):
            count += 1
    return count

def day_4B(res):
    count = 0

    for c in res:
        # separate
        a, b = c
        #create 2 ranges
        range_1 = []
        range_2 = []
        #grab first and second number
        num_1 = ''
        num_2 = ''
        num_3 = ''
        num_4 = ''
        count_1 = False
        count_2 = False

        for i in a:
            if i == '-':
                count_1 = True
            elif i.isdigit() and not count_1:
                num_1 += i
            elif i.isdigit() and count_1:
                num_2 += i
        num_1 = int(num_1)
        num_2 = int(num_2)
        for i in range(num_1, num_2+1):
            range_1.append(i)

        for i in b:
            if i == '-':
                count_2 = True
            elif i.isdigit() and not count_2:
                num_3 += i
            elif i.isdigit() and count_2:
                num_4 += i
        num_3 = int(num_3)
        num_4 = int(num_4)
        for i in range(num_3, num_4+1):
            range_2.append(i)

        # check for overlap between range 1 and 2 
        if any(elem in range_1 for elem in range_2) or any(elem in range_2 for elem in range_1):
            count += 1

    return count


print(day_4A(res))
print(day_4B(res))

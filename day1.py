from aocd import data


def day_1A(data):
    data = data.split('\n')

    temp = 0
    # max_value = 0
    elf_baskets = []

    for calories in data:
        if calories == '':
            elf_baskets.append(temp)
            temp = 0
        else:
            calories = int(calories)
            temp += calories
    return max(elf_baskets)

print(day_1A(data))

def day_1B(data):
    data = data.split('\n')

    temp = 0
    elf_baskets = []

    for calories in data:
        if calories == '':
            elf_baskets.append(temp)
            temp = 0
        else:
            calories = int(calories)
            temp += calories
    elf_baskets.sort(reverse=True)
    return sum(elf_baskets[:3])

print(day_1B(data))

# def day1(data):
    
#     total = 0

#     for i in data:
#         res = count_cal(i)
#         if res > total:
#             total = res
#     return res

# def count_cal(elf):
#     res = 0

#     for food in elf:
#         res += food
#     return res
    
    

# day1(data)

